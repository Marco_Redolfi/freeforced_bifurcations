% ******************************************************************************************************
% Equilibrium solutions of the 1D cell model for a bifurcation with curvature and slope advantage 
% Author: Marco Redolfi
% Matlab version: R2018a
% Open Source code, distributed under GNU General Public Licence (GPLv3)
% NB:
%     *Research code, developed for academic use only!
%     *Not fully tested: please report possible issues at marco.redolfi@unitn.it
%     *All variables are expressed in dimensionless form (i.e. are scaled with the reference flow)
% ******************************************************************************************************

close all
clear all
clc

addpath('Functions')


%% Input parameters

% Basic flow data
theta0=0.1; %Shields stress
ds0=0.02;   %Relative submergence
Delta=1.65; %Relative sumberged density

% Forcing parameters
R=inf;    %Dimensionless radius of curvature (radius/HALF main channel width); inf-> straight channel
rS=1.001; %Slope ratio

% Model data and options
model='RZT';          %'KJMS'-> uniform upstream flow (Kleinhans et al, 2008); 'RZT'-> Redolfi et al.
Dabc_opt=false;       %false-> Da/Dabc is assumed to be 1 (as in Salter et al., 2018)
r=0.5;                %Ikeda coefficient
transport_f='Parker'; %Transport formula MP&M-> Meyer-Peter and Muller (1948); Parker-> Parker (1990)
k=0.41;               %Von Karman constant
epsilon=1;            %Empirical coefficient to reduce the effect of spiral flow (e.g., Kleinhans et al, 2008)

% Geometrical data
ra=1;     %Ratio between main channel width and sum of downstream widths (Wa/(Wb+Wc))
rb=0.5;   %Width of channel B relative to the sum of downstream widths (Wb/(Wb+Wc))

% Range of beta0 values to explore
Nbeta0=150;   %Number of beta0 values to explore
beta0_min=4;  %Minimum value
beta0_max=22; %Maximum value

% Analisys options
calc_evolution=false; %true-> solution calculated through the evolution equation (stable roots only)
calc_newton=true;     %true-> solution calculated using the Newton method (all roots)
calc_linear=false;    %true-> linearized solution, to be plotted over the the nonlinear solutions


%% Creating data structures

% Basic flow data
data_basic=struct('theta0',theta0,'ds0',ds0,'Delta',Delta);

% Geometrical data
data_geometry=struct('ra',ra,'rb',rb,'R',R,'rS',rS);

% Model data and options
data_model=struct('model',model,'Dabc_opt',Dabc_opt,'r',r,'transport_f',transport_f,'k',k,'epsilon',epsilon);


%% Preliminary calculations

% List of beta0 points
beta0=linspace(beta0_min,beta0_max,Nbeta0);
          
% Calculation of the cell length parameter alpha
[data_model.alpha,beta_R]=get_alpha(data_basic,data_geometry,data_model);

% Flow in the upstream Channel A
flow_US=upstream_flow(data_basic,data_geometry,data_model);


%% Computing equilibrium solutions from different methods

% 1) Solution calculated as the asymptotic trend of the evolution equation
if calc_evolution

    %Initial conditions (dimensionless depth)
    IC_E(:,1)=[0.8 1.3];
    IC_E(:,2)=[1.3 0.8];
    Nmax_E=1e5; %Maximum number of iterations in the evolution equation
    tol_E=1E-6; %Tolerance (on rQ variations in a single time step) that defines when equilibrium is attained
    dt=0.1;     %Evolution time step ( D=D+dt*(qs_bOUT-qs_b) )

    for sol=1:2
        rQ_E(:,sol)=evolution_solution(IC_E(1,sol),IC_E(2,sol),beta0,Nmax_E,tol_E,dt,data_basic,data_model,data_geometry,flow_US);
    end
end

% 2) Solution calculated using the Newton method
if calc_newton

    %Initial conditions for Newton solver (dimensionless depth and mean slope)
    IC_S(:,1)=[1 1 flow_US.S0];
    IC_S(:,2)=[1.5 0.5 0.9*flow_US.S0];
    IC_S(:,3)=[0.5 1.5 0.9*flow_US.S0];
    
    Nmax_S = 100;  %Maximum number of iterations 
    tol_S  = 1e-7; %Tolerance (max norm of the residuals)
    for sol=1:3
        rQ_S(:,sol)=newton_solution(IC_S(1,sol),IC_S(2,sol),IC_S(3,sol),Nmax_S,tol_S,beta0,data_basic,data_model,data_geometry,flow_US);
    end
  
end

% 3) Linearized solution
if calc_linear
    rQ_L=linear_solution(beta0,data_basic,data_model,data_geometry,flow_US);
end


%% Plotting

Plotting
