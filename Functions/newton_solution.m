%% Equilibium solution from nonlinear solver
%Author: Marco Redolfi
%Created on: 28-Apr-2017

function rQ=newton_solution(D_b_start,D_c_start,S_bc_start,Nmax,tol,beta0_list,data_basic,data_model,data_geometry,flow_US);

%% Loading parameters

%Geometrical data
rb=data_geometry.rb;
rc=1-rb;
rS=data_geometry.rS;

% Model data and options
transport_f=data_model.transport_f;


%% Calculation

Nbeta0=length(beta0_list);
for j=1:Nbeta0

    beta0=beta0_list(j); %Current beta0 value
    
    fun=@(x) FuncSys(x,beta0,data_basic,data_model,data_geometry,flow_US);

    [sol,exitflag]=newt_solver([D_b_start,D_c_start,S_bc_start],Nmax,tol,fun);
    D_b=sol(1);
    D_c=sol(2);
    S_bc=sol(3);

    %Excluding solutions that are not within the tolerance
    D_b (~exitflag)=NaN;
    D_c (~exitflag)=NaN;
    S_bc(~exitflag)=NaN;
    q_b (~exitflag)=NaN;
    qs_b(~exitflag)=NaN;
    rQ  (~exitflag)=NaN;
    
    S_c=2*S_bc/(1+rS); %Slope of channle C
    S_b=S_c*rS; %Slope of channel B
    
    sol=uniflow(D_b,S_b,data_basic,transport_f);
    q_b=sol(1); qs_b=sol(2);
    sol=uniflow(D_c,S_c,data_basic,transport_f);
    q_c=sol(1); qs_c=sol(2);

    rQ(j)=q_b/q_c*rb/rc; %Discharge ratio
  
end


%% Definition objective functions of the nonlinear system
%NB: we are using dimensionless quantities

function f=FuncSys(x,beta0,data_basic,data_model,data_geometry,flow_US)

%Main input:
%   x(1): Depth in channel B
%   x(2): Depth in channel C
%   x(3): Mean slope of channels B and C 

%Output: residual of three equations
%   Water mass conservation
%   Sediment mass conservation
%   Nodal relation

%% Loading parameters

%Basic flow data
theta0=data_basic.theta0;
ds0   =data_basic.ds0;
Delta =data_basic.Delta;

%Geometrical data
ra=data_geometry.ra;
rb=data_geometry.rb;
rc=1-rb;
R =data_geometry.R;
rS=data_geometry.rS;
H0=0;

% Model data and options
Dabc_opt   =data_model.Dabc_opt;
r          =data_model.r;
alpha      =data_model.alpha;
transport_f=data_model.transport_f;
k          =data_model.k;
epsilon    =data_model.epsilon;

%Upstream flow data
S0     =flow_US.S0;
q_bIN  =flow_US.q_bIN;
qs_bIN =flow_US.qs_bIN;
q_a    =flow_US.q_a;
qs_a   =flow_US.qs_a;
D_a    =flow_US.D_a;
theta_a=flow_US.theta_a;
H_bIN  =flow_US.H_bIN;
H_cIN  =flow_US.H_cIN;


%% Computing main parameters

c0=6.+2.5*log(1/(2.5*ds0));
A=epsilon*2/(k^2)*(1-1/(k*c0));  %Constant in the Struiksma closure for secondary flow

H_b=H_bIN;
H_c=H_cIN;

%% Function definition

D_b= x(1);
D_c =x(2);
S_bc=x(3);

S_c=2*S_bc/(1+rS);
S_b=S_c*rS;

sol=uniflow(D_b,S_b,data_basic,transport_f);
q_b=sol(1); qs_b=sol(2);
sol=uniflow(D_c,S_c,data_basic,transport_f);
q_c=sol(1); qs_c=sol(2);

q_yalpha =q_b *rb/ra-q_bIN *rb;
qs_yalpha=qs_b*rb/ra-qs_bIN*rb;
if Dabc_opt
    D_abc=(2*D_a+D_b+D_c)/4;
else
    D_abc=D_a; %Hp: D_abc/D_a=1
end

eta_b=H_b-D_b;
eta_c=H_c-D_c;

%Water mass conservation
f(1,1)=q_b*rb/ra+q_c*rc/ra-q_a;
%Sediment mass conservation
f(2,1)=qs_b*rb/ra+qs_c*rc/ra-qs_a;
%Nodal relation
f(3,1)=qs_yalpha/qs_a-(q_yalpha/q_a*D_a/D_abc-r*alpha/sqrt(theta_a)*(eta_b-eta_c)*ra/beta0-A*alpha*D_abc/(R*beta0));


return
