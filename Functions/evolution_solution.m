%% Equilibrium solution from long-term temporal evolution
%Author: Marco Redolfi
%Created on: 26-Jun-2017

function [rQ,D_b,D_c,S_bc]=evolution_solution(D_b_init,D_c_init,beta0_list,Nmax,tol,dt,data_basic,data_model,data_geometry,flow_US)

%D_b, D_c: Initial depth values
%beta0: Aspect ratio of the main channel
%Nmax: maximum number of time steps
%tol: tolerance
%dt: evolution time step ( D=D+dt*(qs_bOUT-qs_b) )


%% Loading parameters

%Basic flow data
theta0=data_basic.theta0;
ds0   =data_basic.ds0;
Delta =data_basic.Delta;

% Model data and options
Dabc_opt   =data_model.Dabc_opt;
r          =data_model.r;
alpha      =data_model.alpha;
transport_f=data_model.transport_f;
k          =data_model.k;
epsilon    =data_model.epsilon;

%Geometrical data
ra=data_geometry.ra;
rb=data_geometry.rb;
rc=1-rb;
R =data_geometry.R;
rS=data_geometry.rS;
H0=0;

%Upstream flow data
S0     =flow_US.S0;
q_bIN  =flow_US.q_bIN;
qs_bIN =flow_US.qs_bIN;
q_a    =flow_US.q_a;
qs_a   =flow_US.qs_a;
D_a    =flow_US.D_a;
theta_a=flow_US.theta_a;
H_bIN  =flow_US.H_bIN;
H_cIN  =flow_US.H_cIN;


%% Computing main parameters

c0=6.+2.5*log(1/(2.5*ds0));
A=epsilon*2/(k^2)*(1-1/(k*c0));  %Constant in the Struiksma closure for secondary flow

H_b=H_bIN;
H_c=H_cIN;


%% Calculations

Nbeta0=length(beta0_list);
for i=1:Nbeta0

    beta0=beta0_list(i);
    D_b=D_b_init;
    D_c=D_c_init;

    rQ(i)=Inf;

    for j=1:Nmax

        %% 1) Find slope so that Q is conserved

        %Chézy coefficients
        c_b =6.+2.5*log(D_b/(2.5*ds0));
        c_c =6.+2.5*log(D_c/(2.5*ds0));

        %Downstream slope
        S_b=S0*q_a^2/( rb/ra*c_b/c0*D_b^(3/2) + rc/ra*c_c/c0*D_c^(3/2)/sqrt(rS) )^2;
        S_c=S_b/rS;

        S_bc=(S_b+S_c)/2;


        %% 2) Determine repartition of sediment fluxes

        sol=uniflow(D_b,S_b,data_basic,transport_f);
        q_b=sol(1); qs_b=sol(2);
        sol=uniflow(D_c,S_c,data_basic,transport_f);
        q_c=sol(1); qs_c=sol(2);

        q_yalpha =q_b *rb/ra-q_bIN *rb;

        %Bottom elevation
        eta_b=H_b-D_b;
        eta_c=H_c-D_c;

        %Mean depth of the three channels
        if Dabc_opt
            D_abc=(2*D_a+D_b+D_c)/4;
        else
            D_abc=D_a; %Hp: D_abc/D_a=1
        end
        qs_yalpha=qs_a*(q_yalpha/q_a*D_a/D_abc-r*alpha/sqrt(theta_a)*(eta_b-eta_c)*ra/beta0-A*alpha*D_abc/(R*beta0));

        qs_bOUT=(qs_bIN*rb+qs_yalpha)*ra/rb;
        qs_cOUT=(qs_a-qs_bOUT*rb/ra)*ra/rc;


        %% 3) Update depth

        rQ_prec=rQ(i);

        D_b=D_b-dt*(qs_bOUT-qs_b);
        D_c=D_c-dt*(qs_cOUT-qs_c);
        rQ(i)=q_b/q_c*rb/rc;

        if abs(rQ(i)-rQ_prec)<tol %Stopping criterion
           break 
        elseif j==Nmax
           disp('Warning: maximum # of iterations reached!')
        end
    end
    
end

return
