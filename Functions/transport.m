%% Definition of Phi0 and PhiT=theta0/Phi0 dPhi/dtheta 
%Author: Marco Redolfi
%Created on: 30-Apr-2008

function [Phi0,PhiT]=transport(theta0,transport_f)

switch lower(transport_f)
    
case lower('MP&M')

    theta_cr=0.047;
    
	if theta0<theta_cr
        Phi0=0;
        dPhi_dtheta=0;
    else
        Phi0=8*(theta0-theta_cr)^1.5;
        dPhi_dtheta=8*1.5*(theta0-theta_cr)^(0.5);
    end

case lower('Parker')

     %Parameters
     A=0.0386;
     B=0.853;
     C=5474.;
     D=0.00218;

     x=theta0/A;	%Normalized Shields stress in Parker formula
     if x>1.59
         Phi0=C*D*theta0^1.5*(1.-B/x)^4.5;
         dPhi_dtheta=1.5*theta0^0.5*(1.-B/x)^4.5*C*D+4.5*A*B*(1.-B/x)^3.5*C*D/(theta0^0.5);
     elseif x>1.
         Phi0=D*(theta0^1.5)*(exp(14.2*(x-1.)-9.28*(x-1.)^2));
         dPhi_dtheta=1./A*Phi0*(14.2-9.28*2.*(x-1.)) + 1.5*Phi0/theta0;
     else
         Phi0=D*theta0^1.5*x^14.2;
         dPhi_dtheta=14.2/A*D*theta0^1.5*x^13.2 + D*x^14.2*1.5*theta0^0.5;
     end

     
end

PhiT=theta0/Phi0*dPhi_dtheta;

return