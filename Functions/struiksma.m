%% Flow in the upstream channel according to the Struiksma (non linear) solution
%Author: Marco Redolfi
%Created on: 14-Feb-2018

function flow_US=struiksma(data_basic,data_geometry,data_model)

%% Loading parameters

%Basic flow data
theta0=data_basic.theta0;
ds0   =data_basic.ds0;
Delta =data_basic.Delta;

%Geometrical data
R =data_geometry.R;
rb=data_geometry.rb;
rc=1-rb;
H0=0;

% Model data and options
r          =data_model.r;
transport_f=data_model.transport_f; 
k          =data_model.k;
epsilon    =data_model.epsilon;


%% Computing main parameters

c0=6.+2.5*log(1/(2.5*ds0));
epsilon=0.5; %Empirical coefficient to reduce the effect of secondary flow
A=epsilon*2/(k^2)*(1-1/(k*c0));  %Constant in the Struiksma closure for secondary flow
S0=theta0*Delta*ds0; %Slope of the centerline


%% Calculations

N=100; %Number of steps to integrate the transverse profile

y = linspace(-1,1,N); %Regular grid of N points
%Solving boundary value problem for Q and D
solinit = bvpinit(linspace(-1,1,N),[1 1]);

ode_sys2=@(x,y) ode_sys(x,y,data_basic,data_model,data_geometry);

sol = bvp4c(ode_sys2,@bnd_cond,solinit); %Solution of the differential rwuatuib

f = deval(sol,y); %Evalutating solution at the grid points
Q_ay=f(1,:); %Discharge
D_ay=f(2,:); %Depth 
S_ay=S0*R./(R+y); %Slope

%Calulating sediment transport profile across the section
for i=1:N
    sol=uniflow(D_ay(i),S_ay(i),data_basic,transport_f);
    qs_ay(i)=sol(2);
end    

%Averaged water discharge
q_a=1;
q_bIN=(2-interp1(y,Q_ay,1-2*rb))/(2*rb);
q_cIN=interp1(y,Q_ay,1-2*rb)/(2*rc);

%Averaged sediment transport
Qs_ay=cumtrapz(y,qs_ay);
qs_a=Qs_ay(end)/2;
qs_bIN=(Qs_ay(end)-interp1(y,Qs_ay,1-2*rb))/(2*rb);
qs_cIN=interp1(y,Qs_ay,1-2*rb)/(2*rc);

%Averaged depth and Shields stress
D_a=trapz(y,D_ay)/2;
theta_a=trapz(y,D_ay.*S_ay/(Delta*ds0))/2;

%Averaged water surface elevation
eta_ay=bottom_profile(D_ay,y,ds0,A,r,R,theta0);
H_ay=eta_ay+D_ay;
H_ay=H_ay-trapz(y,H_ay)/2+H0;
H_a=H0;
Hsum=cumtrapz(y,H_ay);
H_bIN=(Hsum(end)-interp1(y,Hsum,1-2*rb))/(2*rb);
H_cIN=interp1(y,Hsum,1-2*rb)/(2*rc);

%Discharge ratio at the cell entrance
rQ_IN=q_bIN/q_cIN*rb/rc;

% Structures containid output data
flow_US   = struct('S0',S0,'q_a',q_a  ,'qs_a',qs_a,  'D_a',D_a,'theta_a',theta_a,'H_a',H_a,...
                   'q_bIN',q_bIN,'qs_bIN',qs_bIN,'H_bIN',H_bIN,...
                   'q_cIN',q_cIN,'qs_cIN',qs_cIN,'H_cIN',H_cIN,...
                   'rQ_IN',rQ_IN);

               
return
               
     
%% Function defining the BCs of the ODE system               
               
function res = bnd_cond(ya,yb)
    res = [ ya(1)+0; yb(1)-2 ]; %Q(a)=0; Q(b)=+2
return


%% Function defining the ODE system               

function dfdy=ode_sys(y,f,data_basic,data_model,data_geometry)

%Basic flow data
theta0=data_basic.theta0;
ds0   =data_basic.ds0;
Delta =data_basic.Delta;

% Model data and options
r      =data_model.r;
k      =data_model.k;
epsilon=data_model.epsilon;

%Geometrical data
R=data_geometry.R;

Q=f(1); %Dimensionless discharge
D=f(2); %Dimensionless depth

%Chézy cooefficient
c =6+2.5*log(D/(2.5*ds0));
c0=6+2.5*log(1/(2.5*ds0));

A=epsilon*2/(k^2)*(1-1/(k*c0));  %Constant in the Struiksma closure for secondary flow
k3=-A/c0;

U=sqrt(R/(R+y))*c/c0*D^(1/2); %Velocity
theta=theta0*D*(R./(R+y)); %Shields stress

dQdy=U*D; %Continuity equation

%y-momentum equation
%   HP:horizontal free surface
dDdy=-D/R*c0*k3*sqrt(theta)/r;

free_surface=true; %false->The free surfaces is assumed to be horizontal

%If the free surface is not horizontal, an additional term arises
if free_surface

    Fr0=sqrt(theta0*Delta*ds0*c0^2);
    %Fr=Fr0*U/sqrt(D);

    dHdy=Fr0^2*(U^2/R-U^2*(c0/c^2)*k3/R); %Derivative of the free surface elevation

    dDdy=dDdy+dHdy;

end

dfdy=[dQdy;dDdy];

return


%% Function for caluclating the transverse bed elevation profile

function eta=bottom_profile(D,y,ds0,A,r,R,theta0)
	
    %Chézy cooefficient
    c=6+2.5*log(D/(2.5*ds0));
    c0=6+2.5*log(1/(2.5*ds0));
    
    k3=-A/c0;
    
    U=sqrt(R./(R+y)).*c./c0.*D.^(1/2); %Velocity
    theta=theta0*D.*(R./(R+y)); %Shields stress
    
    detady=D/R.*c0*k3.*sqrt(theta)/r;
    
    eta=cumtrapz(y,detady);
        
return
    
  
