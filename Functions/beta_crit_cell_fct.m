%% Critical aspect ratio of the two-cells model
%NB: general solution for arbitrary channel width (rb~=rc, ra~1)
%Author: Marco Redolfi
%Created on: 27-Apr-2018

function beta_C=beta_crit_cell_fct(data_basic,data_geometry,data_model)

%% Loading parameters

%Basic flow data
theta0=data_basic.theta0;
ds0   =data_basic.ds0;
Delta =data_basic.Delta;

%Geometrical data
ra=data_geometry.ra;
rb=data_geometry.rb;
rc=1-rb;

% Model data and options
Dabc_opt   =data_model.Dabc_opt;
r          =data_model.r;
alpha      =data_model.alpha;
transport_f=data_model.transport_f;


%% Basic flow parameters

if ra==1 %If the sum of downstream width is different then the main channel width

    %Upstream basic flow parameters
    c0=6.+2.5*log(1/(2.5*ds0));
    cD=2.5/c0; 
    [~,PhiT]=transport(theta0,transport_f);

    %Downstream parameters=upstream parameters
    cD_bc=cD;
    PhiT_bc=PhiT;
    D0_bc=1;
    D0_abc=1;

else
    
    %Unit water and sediment discharges
    q0_bc=ra;
    qs0_bc=ra;
    
    %Water depth and slope that gives the downstream values of q0 and qs0
    fun=@(x) uniflow(x(1),x(2),data_basic,transport_f)-[q0_bc,qs0_bc]';
    S0=theta0*Delta*ds0; 
    sol=newt_solver([1,S0],1000,1E-6,fun); 
    D0_bc=sol(1); S0_bc=sol(2);

    ds_bc=ds0/D0_bc;
    c0_bc=6.+2.5*log(1/(2.5*ds_bc));
    cD_bc=2.5/c0_bc;
    theta0_bc=S0_bc/(Delta*ds_bc);

    [~,PhiT_bc]=transport(theta0_bc,transport_f);

    if Dabc_opt
        D0_abc=(1+D0_bc)/2;
    else
        D0_abc=1; %Hp: D_abc/D_a=1
    end

end   

%% Critical aspect ratio

beta_C=r*alpha/sqrt(theta0)*ra*(1+rb/rc)/( (PhiT_bc-1/D0_abc*(3+2*cD_bc)/2)/D0_bc*rb );

return
