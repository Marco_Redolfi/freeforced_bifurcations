%% Linear solution
%Author: Marco Redolfi
%Created on: 28-Apr-2017

function rQ_L=linear_solution(beta_list,data_basic,data_model,data_geometry,flow_US)

%% Loading parameters

%Basic flow data
theta0=data_basic.theta0;
ds0   =data_basic.ds0;
Delta =data_basic.Delta;

%Model data and options
Dabc_opt   =data_model.Dabc_opt;
r          =data_model.r;
alpha      =data_model.alpha;
transport_f=data_model.transport_f;
k          =data_model.k;
epsilon    =data_model.epsilon;

%Geometrical data
ra=data_geometry.ra;
rb=data_geometry.rb;
rc=1-rb;
R=data_geometry.R;
rS=data_geometry.rS;
H0=0;

%Upstream flow data
S0    =flow_US.S0;
q_bIN =flow_US.q_bIN;
qs_bIN=flow_US.qs_bIN;
q_a   =flow_US.q_a;
qs_a  =flow_US.qs_a;
H_bIN =flow_US.H_bIN;
H_cIN =flow_US.H_cIN;


%% Perturbed quantities

q1_bIN= q_bIN-1;
qs1_bIN=qs_bIN-1; 

q1_a= q_a-1;
qs1_a=qs_a-1; 

H_b=H_bIN;
H_c=H_cIN;

H1_b=H_b-H0;
H1_c=H_c-H0;

rS1=rS-1; %Slope ratio perturbation


%% Secondary flow parameter A

c0=6.+2.5*log(1/(2.5*ds0));
A=epsilon*2/(k^2)*(1-1/(k*c0));  %Constant in the Struiksma closure for secondary flow

    
%% Basic (i.e. unoerturbed) flow in the downstream anabranches


if ra==1 %If the sum of downstream width is different then the main channel width

    cD=2.5/c0;
    [~,PhiT]=transport(theta0,transport_f);

    %Basic flow is the same as in the upstream channels
    q0_bc=1;  %unit water discharge
    qs0_bc=1; %unit sediment discharge
    D0_bc=1;  
    S0_bc=S0;
    ds_bc=ds0;
    c0_bc=c0;
    cD_bc=cD;
    theta0_bc=theta0;
    PhiT_bc=PhiT;
    D0_abc=1; 


else

    %Unit discharges are different than in the main channel
    q0_bc=ra;
    qs0_bc=ra;

    %Water depth and slope that gives the downstream values of q0 and qs0
    fun=@(x) uniflow(x(1),x(2),data_basic,transport_f)-[q0_bc,qs0_bc]';
    sol=newt_solver([1,S0],1E3,1E-6,fun); 
    D0_bc=sol(1); S0_bc=sol(2);

    ds_bc=ds0/D0_bc;
    c0_bc=6.+2.5*log(1/(2.5*ds_bc));
    cD_bc=2.5/c0_bc;
    theta0_bc=S0_bc/(Delta*ds_bc);

    [~,PhiT_bc]=transport(theta0_bc,transport_f);

    if Dabc_opt
        D0_abc=(1+D0_bc)/2;
    else
        D0_abc=1; %Hp: D_abc/D_a=1
    end

end


%% Solution of the linear system

Nbeta=length(beta_list);
for j=1:Nbeta

    beta=beta_list(j);

    %Linear system M*S=F;

    %Matrix of the linear system

    %Continuty equation (Qa=Qb+Qc)
    M(1,:)= [(3+2*cD_bc)/2/D0_bc*rb , (3+2*cD_bc)/2/D0_bc*rc , +1/2/S0_bc ];

    %Sediment continuity equation (Qsa=Qsb+Qsc)
    M(2,:)= [PhiT_bc/D0_bc      *rb , PhiT_bc /D0_bc    *rc  , +PhiT_bc/S0_bc]; 

    %Nodal point relation
    M(3,1)= (PhiT_bc-1/D0_abc*(3+2*cD_bc)/2)/D0_bc*rb-r*alpha/(beta*sqrt(theta0))*ra ; 
    M(3,2)= r*alpha/(beta*sqrt(theta0))*ra; 
    M(3,3)= PhiT_bc/S0_bc*rb-1/D0_abc*1/2/S0_bc*rb; 


    % Vecor of known (forcing) terms

    F(1)=q1_a-1/4*rS1*(rb-rc);
    F(2)=qs1_a-1/2*PhiT_bc*rS1*(rb-rc);
    F(3)=-A*alpha*D0_abc/(R*beta)-1/2*PhiT_bc*rS1*rb+1/4*rS1*rb + qs1_bIN*rb - q1_bIN*rb - r*alpha/(beta*sqrt(theta0))*ra*(H1_b-H1_c);

    %Solution of the linear system
    S=inv(M)*F';
    D1_b(j) =S(1); %Discharge perturbation in channel B
    D1_c(j) =S(2);
    S1_bc(j)=S(3);


end

%Unit water discharges in the downstream anabranches
q1_b=q0_bc*(3+2*cD_bc)/2*D1_b/D0_bc+q0_bc*1/2*(S1_bc/S0_bc+1/2*rS1);
q1_c=q1_a*ra/rc-q1_b*rb/rc;

%Discharge ratio
rQ_L=(q0_bc+q1_b)./(q0_bc+q1_c)*rb/rc; %Discharge ratio

return
