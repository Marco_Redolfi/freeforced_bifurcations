%% Dimensionless unit discharge (water and sediment) as a function of depth and slope
%Author: Marco Redolfi
%Created on: 26-Jun-2017

function res=uniflow(D,S,data_basic,transport_f)

    %Basic flow data
    theta0=data_basic.theta0;
    ds0   =data_basic.ds0;
    Delta =data_basic.Delta;
    
	c0=6.+2.5*log(1/(2.5*ds0)); %Chézy coefficient of the basic flow
	c =6.+2.5*log(D/(2.5*ds0)); %Chézy coefficient
    S0=theta0*Delta*ds0; %Reference slope
    
    %1) Dimensionless water discharge
    if D/ds0<2.5*exp(-6/2.5);
        q=0;
    else
        q = c/c0*D^(3/2)*sqrt(S/S0); %Chezy formula   
    end
       
    %2) Dimensionless sediment discharge
    
    theta=S*D/(Delta*ds0); %Shields stress (uniform flow)
    Phi=transport(theta,transport_f);
    Phi0=transport(theta0,transport_f);
    qs=Phi/Phi0; %Dimensionless sediment flux
    
    res=[q qs]; %Vector of results
    
return
