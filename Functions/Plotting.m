%% Script for plotting discharge asymmetry as a function of aspect ratio
%Author: Marco Redolfi

if calc_linear

    DeltaQ_L=(rQ_L-1)./(rQ_L+1); %From discharge ratio to discharge asymmetry

    %Index at which the linear solution changes sign (crossing singular point at resonance)
    ind=find(diff(sign(DeltaQ_L))~=0); 

end

%% 1) Plotting results from evolution equation

if calc_evolution
    
    DeltaQ_E=(rQ_E-1)./(rQ_E+1); %From discharge ratio to discharge asymmetry
    
    figure('Name','Linear and Evolution solutions')
        plot(beta0, DeltaQ_E(:,1),'.r',beta0,DeltaQ_E(:,2),'.b');

        if calc_linear
            hold on
                plot(beta0(1:ind), DeltaQ_L(1:ind),'-k');
                plot(beta0(ind+1:end), DeltaQ_L(ind+1:end),'-k');
            hold off
        end 
        grid on
        xlabel('\beta_0')
        ylabel('\DeltaQ=(Q_B-Q_C)/Q_A')
        legend('Solution 1','Solution 2','Linearised solution','Location','NW')
        xlim([min(beta0) max(beta0)])
        ylim([-0.6 0.6])

        printpdf(['Figures/Evolution_',model,'.pdf'],[0 0.2 0.2 0],gcf,[15,10])

end

%% 2) Plotting results from Newton solver

if calc_newton
    
    DeltaQ_S=(rQ_S-1)./(rQ_S+1); %From discharge ratio to discharge asymmetry
    
    figure('Name','Steady solutions')
        pl1=plot(beta0, DeltaQ_S(:,1),'.m',beta0,DeltaQ_S(:,2),'.r',beta0,DeltaQ_S(:,3),'.b');
                
        if calc_linear
            hold on    
                plot(beta0(1:ind), DeltaQ_L(1:ind),'-k');
                plot(beta0(ind+1:end), DeltaQ_L(ind+1:end),'-k');
            hold off
        end
        xlim([min(beta0) max(beta0)])
        ylim([-0.6 0.6])
        grid on
        xlabel('\beta_0')
        ylabel('\DeltaQ=(Q_B-Q_C)/Q_A')
        legend('Solution 1','Solution 2','Solution 3','Linearised solution','Location','NW')
      
        printpdf(['Figures/Newton_',model,'.pdf'],[0 0.2 0.2 0],gcf,[15,10])

end

