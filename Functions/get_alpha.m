%% Getting the cell length parameter alpha
%Author: Marco Redolfi
%Created on: 10-May-2018

function [alpha,beta_R]=get_alpha(data_basic,data_geometry,data_model)

%Basic flow data
Delta =data_basic.Delta;

%Model data and options
transport_f=data_model.transport_f;
r          =data_model.r;

%Critical beta when assuming alpha=1
data_model.alpha=1;
beta_C=beta_crit_cell_fct(data_basic,data_geometry,data_model);

%Resonant beta
beta_R=beta_res_fct(data_basic.theta0,data_basic.ds0,'r',r,'transport_f',transport_f,'Delta',Delta);

%alpha that makes beta_C=beta_R (considering that beta_C increases propotionally with alpha)
alpha=beta_R/beta_C;

return

