%% Flow in the upstream channel depending on the model
%Author: Marco Redolfi
%Created on: 14-Feb-2018

function flow_US=upstream_flow(data_basic,data_geometry,data_model)

%Geometrical data
R=data_geometry.R;

% Model data and options
model=data_model.transport_f;

if strcmp(model,'KJMS') || isinf(R) %Unperturbed upstream flow
    
    %Basic flow data
    theta0=data_basic.theta0;
    ds0   =data_basic.ds0;
    Delta =data_basic.Delta;
    
    %Geometrical data
    rb=data_geometry.rb;
    rc=1-rb;
    
    H0=0; %Reference elevation
    S0=theta0*Delta*ds0; %Reference slope

    %Unperturbed solution
    q_a=1;   qs_a=1; 
    D_a=1;   H_a=H0; theta_a=theta0;
    q_bIN=1; qs_bIN=1; H_bIN=H0;
    q_cIN=1; qs_cIN=1; H_cIN=H0;
    
    %Discharge ratio at the cell entrance
    rQ_IN=rb/rc;
    
    % Structures containid output data
    flow_US=struct('S0',S0,'q_a',q_a ,'qs_a',qs_a,'D_a',D_a,'theta_a',theta_a,'H_a',H_a,...
                   'q_bIN',q_bIN,'qs_bIN',qs_bIN,'H_bIN',H_bIN,...
                   'q_cIN',q_cIN,'qs_cIN',qs_cIN,'H_cIN',H_cIN,...
                   'rQ_IN',rQ_IN);
    
else %Non uniform upstream flow
     
    flow_US=struiksma(data_basic,data_geometry,data_model);
   
end
