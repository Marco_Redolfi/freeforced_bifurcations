%% Globally convergent Newton method for general N x N systems 

function [x,exitflag]=newt_solver(x_start,Nmax,tol,FuncSys)
    
x=x_start;
if size(x,1)==1 %Convert row vector in column vector if needed
   x=transpose(x); 
end

exitflag=false;

for iter=[1:Nmax]
   
    f  = FuncSys(x);    %Evaluate the nonlinear function 
    res = norm(f);      %Compute the residual of the nonlinear function
   
    if(res<tol)         %If tolerance has been reached, stop iterations 
        exitflag=true;
        break;
    end
    DF = DFuncSys(x,FuncSys);   %Compute the derivative of the function 
    dx = -DF\f;         %Solve the linear equation system 

    delta = 1;          %Always try an entire Newton step 
    for j=1:6
        if(norm(FuncSys(x+delta*dx))<norm(FuncSys(x)))
            break; 
        else
            delta = delta*0.1;  %If the residual does not decrease 
        end
    end
    x = x + delta*dx;    
   
end
  
return


%% Numerical derivative of a vector function

function df = DFuncSys(z,FuncSys) 
    N = length(z); 
    epsilon = 1e-7; 
    df = zeros(N,N); 
    for j=[1:N]
        zp = z; 
        zp(j) = zp(j) + epsilon; 
        zm = z; 
        zm(j) = zm(j) - epsilon; 
        df(:,j) = (FuncSys(zp)-FuncSys(zm))/(2*epsilon); 
    end
return
